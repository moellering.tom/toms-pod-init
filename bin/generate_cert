#! /bin/bash

#
# Handle input args:
#
cert_base_file_name=''
cert_internal_hostname=''
cert_hostname=''

function show_usage() {
    echo ""
    echo "usage: generate_cert -e {Env: dev, qa, prod} -c {opt: Cert Host Name} -p {opt: Cert Path} -f {opt: Cert Filename} -k {opt: k8s internal hostname}"
    printf "\n\t example: generate_cert -e dev\n"
    printf "\n\t example: generate_cert -e dev -p '/app/certs/' -f 'server.cert'\n"
    printf "\n\t dry run: generate_cert -e dev -p '/app/certs/' -f 'server.cert' -d 'true'\n"
    printf "\n\t get help: generate_cert -h\n"
    echo ""
}

function eval_parameters() {
    if [ -z "$environment" ]
    then
        show_usage
        exit 255
    fi

    if [ "$environment" != "dev" ] && [ "$environment" != "qa" ] && [ "$environment" != "prod" ]
    then
        echo "[ERROR] Input parameter '-e' must have a value of: dev, qa, or prod"
        show_usage
        exit 255
    fi

    cert_internal_hostname="$environment.internal.pickupnow.com"

    if [ "$environment" != "prod" ]
    then
        cert_hostname="$environment.pickupnow.com"
    else
        cert_hostname="pickupnow.com"
    fi

    if [ -z "$cert_path" ]
    then
        cert_path='/app/certs/'
    fi

    if [ -z "$cert_file_name" ]
    then
        cert_file_name='server.crt'
    fi

    if [ -z "$k8s_internal_hostname" ]
    then
        k8s_internal_hostname='svc.cluster.local'
    fi

    # create computed input params
    get_cert_base_name

    show_parameters
}

function show_parameters() {
    echo ""
    echo "[generate_cert] is using parameters:"
    echo "  Environment: $environment"
    echo "  Cert Hostname: $cert_hostname"
    echo "  Cert Internal Hostname: $cert_internal_hostname"
    echo "  Cert Path: $cert_path"
    echo "  Cert File Name: $cert_file_name (cert base file name: $cert_base_file_name)"
    echo "  K8s internal hostname: $k8s_internal_hostname"
    echo ""
}

function get_cert_base_name() {
    cert_base_file_name=$(echo "$cert_file_name" | cut -d '.' -f 1)
}

while getopts e:p:f:k:d: flag
do
    case "${flag}" in
        e) environment=${OPTARG};;
        p) cert_path=${OPTARG};;
        f) cert_file_name=${OPTARG};;
        k) k8s_internal_hostname=${OPTARG};;
        d) dry_run=${OPTARG};;
    esac
done

#
# cert generation
#
function create_cert_dir() {
    mkdir -p "$cert_path" # creates only if missing
}

function generate_cert() {
    echo "[INFO] openssl version:"
    openssl version
    echo ""

    echo "[INFO] Generating cert..."
    SUBJECT="$cert_hostname"
    openssl req \
    -x509 \
    -newkey rsa:4096 \
    -sha512 \
    -days 3650 \
    -nodes \
    -keyout "$cert_path$cert_base_file_name.key" \
    -out "$cert_path$cert_base_file_name.crt" \
    -subj "/CN=${SUBJECT}" \
    -extensions v3_ca \
    -extensions v3_req \
    -config <( \
    echo '[req]'; \
    echo 'default_bits= 4096'; \
    echo 'distinguished_name=req'; \
    echo 'x509_extension = v3_ca'; \
    echo 'req_extensions = v3_req'; \
    echo '[v3_req]'; \
    echo 'basicConstraints = CA:FALSE'; \
    echo 'keyUsage = nonRepudiation, digitalSignature, keyEncipherment'; \
    echo 'subjectAltName = @alt_names'; \
    echo '[ alt_names ]'; \
    echo "DNS.1 = ${SUBJECT}"; \
    echo "DNS.2 = *.${SUBJECT}"; \
    echo "DNS.3 = *.$k8s_internal_hostname"; \
    echo "DNS.4 = $cert_internal_hostname"; \
    echo "DNS.5 = *.$cert_internal_hostname"; \
    echo "DNS.6 = localhost"; \
    echo "DNS.7 = localhost.pickupnow.com"; \
    echo '[ v3_ca ]'; \
    echo 'subjectKeyIdentifier=hash'; \
    echo 'authorityKeyIdentifier=keyid:always,issuer'; \
    echo 'basicConstraints = critical, CA:TRUE, pathlen:0'; \
    echo 'keyUsage = critical, cRLSign, keyCertSign'; \
    echo 'extendedKeyUsage = serverAuth, clientAuth')

    echo "[INFO] Create 'pfx' from cert+key..."
    openssl pkcs12 -export -out "$cert_path$cert_base_file_name.pfx" -inkey "$cert_path$cert_base_file_name.key" -in "$cert_path$cert_base_file_name.crt" -passout pass:
    openssl pkcs12 -export -out "$cert_path$cert_base_file_name.alt.pfx" -inkey "$cert_path$cert_base_file_name.key" -in "$cert_path$cert_base_file_name.crt" -passout pass:empty

    # fix cert pemissions
    chmod 644 "$cert_path$cert_base_file_name.crt"
    chmod 644 "$cert_path$cert_base_file_name.pfx"
    chmod 644 "$cert_path$cert_base_file_name.alt.pfx"
    chmod 600 "$cert_path$cert_base_file_name.key"

    # output cert scripts
    cp /usr/local/bin/cert_import "$cert_path"
}

#
# Do stuff / 'main'
#
eval_parameters
create_cert_dir

if [ -z "$dry_run" ] || [ "$dry_run" != "true" ]
then
    generate_cert
fi
