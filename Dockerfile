FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y apt-transport-https ca-certificates curl unzip jq openssl

# install bin scripts/utilities
COPY bin/* /usr/local/bin/

# install test collateral
COPY test/* /test/

WORKDIR /app

CMD [ "/bin/bash" ]
