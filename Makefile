.PHONY: build version set-executable

IMAGE ?= toms-pod-init
TAG ?= latest

build: set-executable
	docker build --tag $(IMAGE):$(TAG) .

version:
	@echo OpenSSL version...
	@docker run --rm $(IMAGE):$(TAG) openssl version -a

exec:
	docker run --rm -it \
	$(IMAGE):$(TAG)

run-tests:
	docker run --rm \
	$(IMAGE):$(TAG) \
	/test/run_tests.sh

build-and-test: build run-tests

set-executable:
	chmod +x bin/*
	chmod +x test/*.sh
