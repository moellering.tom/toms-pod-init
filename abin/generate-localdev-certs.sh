#! /bin/bash

#
# Handle input args:
#
docker_image=''
docker_tag=''
cert_path=''

function show_usage() {
    echo ""
    echo "usage: setup-localdev-certs -c {opt: cert file path} -d {opt: docker image} -t {opt: gitlab tag}"
    printf "\n  example: setup-localdev-certs.sh"
    printf "\n  example: setup-localdev-certs.sh -c \"/app/certs\""
    printf "\n  example: setup-localdev-certs.sh -d \"registry.gitlab.com/pickupnow/docker/pod-init-utilities\""
    printf "\n  example: setup-localdev-certs.sh -t latest"
    printf "\n\n  get help: setup-localdev-certs.sh -help\n"
    echo ""
}

function eval_parameters() {
    if [ -z "$cert_path" ]
    then
        cert_path='/app/certs'
    fi

    if [ -z "$docker_image" ]
    then
        docker_image='registry.gitlab.com/pickupnow/docker/pod-init-utilities'
    fi

    if [ -z "$docker_tag" ]
    then
        docker_tag='latest'
    fi

    show_parameters
}

function show_parameters() {
    echo ""
    echo "[setup-localdev-certs] is using parameters:"
    echo "  Cert Path: $cert_path"
    echo "  Docker Image: $docker_image"
    echo "  Docker Tag: $docker_tag"
    echo ""
}

#
# Setup certs
#
function create_certs() {
    echo "[INFO] openssl version:"
    openssl version
    echo "[INFO] Creating cert directrory if necessary: $cert_path"
    mkdir -p "$cert_path" # creates only if missing
    docker run -v "$cert_path:/app/certs" --rm "$docker_image:$docker_tag" bash -c  "generate_cert -e dev; chmod 644 /app/certs/*"

    # If you're using WSL **AND** you want to use Visual Studio (or Rider) then we need to also copy the pfx cert
    # into the windows file system:
    if [ -d "/mnt/c" ]
    then
        echo "[INFO] Environment appears to be Windows Subsystem for Linux, copying certs into Windows..."
        mkdir -p "/mnt/c$cert_path/"
        cp "$cert_path/server.crt" "/mnt/c$cert_path/"
        cp "$cert_path/server.key" "/mnt/c$cert_path/"
        cp "$cert_path/server.pfx" "/mnt/c$cert_path/"
    fi
}

function test_certs_exist() {
    if [ ! -f "$cert_path/server.pfx" ]
    then
        exit 255
    fi
}

#
# Do stuff / 'main'
#

# handle inputs
while getopts c:d:t:h: flag
do
    case "${flag}" in
        c) cert_path=${OPTARG};;
        d) docker_image=${OPTARG};;
        t) docker_tag=${OPTARG};;
        h) show_usage;;
        *) show_usage;;
    esac
done

eval_parameters

# create certs (if needed)
echo "[INFO] Setting up dev certificates at path: '$cert_path'"
echo ""

if [ ! -f "$cert_path/server.pfx" ]
then
    create_certs
    test_certs_exist
else
    echo "[INFO] The certificates already exist at path '$cert_path', nothing to do..."
    echo ""
fi
