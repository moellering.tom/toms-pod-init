#! /bin/bash

echo ""
echo "Installing bats..."
apt install -y git
git clone --depth=1 https://github.com/basherpm/basher.git ~/.basher
export PATH="$HOME/.basher/bin:$PATH"
basher install bats-core/bats-core
export PATH="$HOME/.basher/cellar/bin:$PATH"

echo ""
echo ""
echo "Bats Version:"
bats --version
