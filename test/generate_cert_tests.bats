#! bats

default_cert_path="/app/certs/"
default_cert_filename="server.crt"
default_k8s_internal_hostname="svc.cluster.local"

function test_certs_exist() {
    pfx_filename="server.pfx"

    if [ -f "$default_cert_path$default_cert_filename" ] && [ -f "$default_cert_path$pfx_filename" ]
    then
        echo "true"
    else
        echo "false"
    fi
}

function validate_cert() {
    cert_filename="server.key"
    pfx_filename="server.pfx"

    openssl version
    openssl x509 -noout -text -in "$default_cert_path$default_cert_filename"
    openssl rsa -noout -text -in "$default_cert_path$cert_filename"
    openssl pkcs12 -noout -info -in "$default_cert_path$pfx_filename" -passin pass:
}

function validate_cert_contains_sna() {
    cert_info=$(openssl x509 -noout -text -in "$default_cert_path$default_cert_filename")
    result="true"

    local -n arr=$1

    for i in "${arr[@]}"
    do
        if [[ "${cert_info}" != *"$i"* ]]
        then
            echo "Failed finding: '$i' in '$cert_info'"
            result="false"
        fi
    done

    echo $result
}

@test 'generate_cert should fail with no input parameters' {
    run generate_cert
    echo "$output"
    [ "${status}" -ne 0 ]
}

@test 'generate_cert runs with valid required parameter' {
    run generate_cert -e dev -d 'true'
    echo "$output"
    [ "${status}" -eq 0 ]
    [[ "${output}" == *"$default_cert_path"* ]]
    [[ "${output}" == *"$default_cert_filename"* ]]
    [[ "${output}" == *"$default_k8s_internal_hostname"* ]]
    [[ "${output}" == *"dev.pickupnow.com"* ]]
    [[ "${output}" == *"dev.internal.pickupnow.com"* ]]
    [[ "${output}" == *"dev"* ]]
    [ $(test_certs_exist) == "false" ]

    run generate_cert -e qa -d 'true'
    echo "$output"
    [ "${status}" -eq 0 ]
    [[ "${output}" == *"$default_cert_path"* ]]
    [[ "${output}" == *"$default_cert_filename"* ]]
    [[ "${output}" == *"$default_k8s_internal_hostname"* ]]
    [[ "${output}" == *"qa.pickupnow.com"* ]]
    [[ "${output}" == *"qa.internal.pickupnow.com"* ]]
    [[ "${output}" == *"qa"* ]]
    [ $(test_certs_exist) == "false" ]

    run generate_cert -e prod -d 'true'
    echo "$output"
    [ "${status}" -eq 0 ]
    [[ "${output}" == *"$default_cert_path"* ]]
    [[ "${output}" == *"$default_cert_filename"* ]]
    [[ "${output}" == *"$default_k8s_internal_hostname"* ]]
    [[ "${output}" == *"pickupnow.com"* ]]
    [[ "${output}" == *"prod.internal.pickupnow.com"* ]]
    [[ "${output}" != *"dev.pickupnow.com"* ]]
    [[ "${output}" != *"qa.pickupnow.com"* ]]
    [[ "${output}" != *"dev.internal.pickupnow.com"* ]]
    [[ "${output}" != *"qa.internal.pickupnow.com"* ]]
    [[ "${output}" == *"prod"* ]]
    [ $(test_certs_exist) == "false" ]
}

@test 'generate_cert fails with invalid required parameter' {
    run generate_cert -e foo
    echo "$output"
    [ "${status}" -ne 0 ]
}

@test 'generate_cert with all parameters reports expected input values' {
    run generate_cert -e qa -p '/foo/bar/' -f 'baz.cert' -k 'foo.bar.baz' -d 'true'
    echo "$output"

    [ "${status}" -eq 0 ]
    [[ "${output}" == *"/foo/bar/"* ]]
    [[ "${output}" == *"baz.cert"* ]]
    [[ "${output}" == *"foo.bar.baz"* ]]
    [[ "${output}" == *"qa"* ]]

    # verify defaults are not included
    [[ "${output}" != *"$default_cert_path"* ]]
    [[ "${output}" != *"$default_cert_filename"* ]]
    [[ "${output}" != *"$default_k8s_internal_hostname"* ]]
}

@test 'generate_cert creates (if needed) the cert folder for cert path' {
    run generate_cert -e dev -d 'true'
    echo "$output"
    [ "${status}" -eq 0 ]
    [ -d "$default_cert_path" ]
    [ $(test_certs_exist) == "false" ]
}

@test 'generate_cert creates the requested cert' {
    run generate_cert -e dev

    echo "$output"
    [ "${status}" -eq 0 ]
    [ $(test_certs_exist) == "true" ]
}

@test 'generate_cert creates valid certs' {
    run generate_cert -e dev
    run validate_cert

    echo "$output"
    [ "${status}" -eq 0 ]
}

@test 'generate_cert creates dev cert with expected sna entries' {
    run generate_cert -e dev
    declare -a dev_sna=("DNS:*.dev.pickupnow.com" "DNS:*.dev.internal.pickupnow.com" "DNS:*.svc.cluster.local")
    run validate_cert_contains_sna dev_sna

    echo "$output"
    [ "${status}" -eq 0 ]
    [ "${output}" == "true" ]
}

@test 'generate_cert creates qa cert with expected sna entries' {
    run generate_cert -e qa
    declare -a dev_sna=("DNS:*.qa.pickupnow.com" "DNS:*.qa.internal.pickupnow.com" "DNS:*.svc.cluster.local")
    run validate_cert_contains_sna dev_sna

    echo "$output"
    [ "${status}" -eq 0 ]
    [ "${output}" == "true" ]
}

@test 'generate_cert creates prod cert with expected sna entries' {
    run generate_cert -e prod
    declare -a dev_sna=("DNS:*.pickupnow.com" "DNS:*.prod.internal.pickupnow.com" "DNS:*.svc.cluster.local")
    run validate_cert_contains_sna dev_sna

    echo "$output"
    [ "${status}" -eq 0 ]
    [ "${output}" == "true" ]
}
