#! /bin/bash

/test/install_bats.sh
export PATH="$HOME/.basher/cellar/bin:$PATH"

#
# tests for: generate_cert
#
printf "\n[TESTING] generate_cert \n"
bats /test/generate_cert_tests.bats
