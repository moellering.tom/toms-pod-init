# pod-init-utilities

This is a light-weight utility container that's meant to be used as a `pod-init` container in `k8s`. See the _included tools_ list below for more information about which utilities are included by default in this container.

## Included Tools

| Tool | Description |
| ---  | ---         |
| [generate_cert](bin/generate_cert) | Generate a random self-signed certificate |

## Releasing New Images

To make changes to the published image, allow your changes to run through a normal pipeline,
then head to the gitlab page and create a new release. This will generate a tag which will kick
a pipeline to tag the image with the release's tag name. You can now use this tag as your base
image for other `Dockerfile`s.
